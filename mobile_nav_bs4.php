<?php

?>
<nav class="navbar navbar-expand-md navbar-inverse" style="background-color: #669900; border: 0;">
    <div class="container">
    <a href="./index.php" class="navbar-brand">
        <img src="./images/favicon_cactus-32x32.png" height="32" alt="CACTU$ Costing"> <?php echo("$group_title $running_title"); ?>
    </a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav">

        </div>
        <div class="navbar-nav ml-auto"  >
            <a  href="./admin_index.php" class="nav-item nav-link" style="color: antiquewhite !important;">Admin</a>
        </div>
    </div>
    </div>
</nav>
<!--
<nav id="myNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" style="background-color: #669900; border: 0;"  role="navigation">

    <div class="container">
        <div class="navbar-header">
            <A class="navbar-brand" href="./index.php"><?php echo("$group_title $running_title"); ?></A>
        </div>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" style="background-color: #669900;" id="navbarCollapse">
            <div class="navbar-nav">
                <a href="#" class="nav-item nav-link active">Home</a>
            </div>

            <div class="navbar-nav ml-auto">
                    <a style="background-color: #669900;" href="admin_index.php">Admin</a>
            </div>

        </div>
    </div>
</nav>
-->